package com.tutoratose1920.jexplorer.file.stub;

import java.io.File;
import java.io.IOException;
import java.io.Reader;

import com.tutoratose1920.jexplorer.file.filesystem.FileSystemInterface;

public class TestFSInterface implements FileSystemInterface {

    @Override
    public void openRoot(String path) throws IOException {
        if (path.contains(("invalid"))) {
            throw new IOException();
        }

        if (path.contains(("file"))) {
            throw new IOException();
        }
    }

    @Override
    public boolean isFile(String path) {
        return false;
    }

    @Override
    public boolean isDirectory(String path) {
        return false;
    }

    @Override
    public String getIdentifier(String path) {
        return null;
    }

    @Override
    public String[] getChildren(String path) {
        return null;
    }

    @Override
    public Reader getReader(String path) throws IOException {
        return null;
    }
    
}