/*
    Dato un numero intero in input, codificarlo in base due usando gli stream (senza utilizzare metodi di conversione standard).
    Dopodiché applicare allo stream di bit la CODIFICA del gigabit ethernet. (Parlo della codifica, NON della decodifica di 
    Viterbi). In output mostrare la stringa di bit ottenuta.
*/

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Stream;
import java.util.stream.IntStream;
import java.util.stream.Collectors;
import java.util.Random;
import java.util.Arrays;

public class Exer6 {
    private static class State {
        protected State zeroNextState, oneNextState;
        protected byte zeroOutputSymbol, oneOutputSymbol;
        protected String id;

        public String toString() {
            return "[" + id + ", " + zeroNextState.id + ", " + zeroOutputSymbol + ", " + oneNextState.id + ", " + oneOutputSymbol + "]";
        }
    }

    private static State 
                  q00 = new State(),
                  q01 = new State(),
                  q10 = new State(),
                  q11 = new State();

    private static void initStates() {
        q00.zeroNextState = q00;
        q00.zeroOutputSymbol = 0b00;
        q00.oneNextState = q10;
        q00.oneOutputSymbol = 0b11;
        q00.id = "00";

        q01.zeroNextState = q00;
        q01.zeroOutputSymbol = 0b11;
        q01.oneNextState = q10;
        q01.oneOutputSymbol = 0b00;
        q01.id = "01";

        q10.zeroNextState = q01;
        q10.zeroOutputSymbol = 0b10;
        q10.oneNextState = q11;
        q10.oneOutputSymbol = 0b01;        
        q10.id = "10";
        
        q11.zeroNextState = q01;
        q11.zeroOutputSymbol = 0b01;
        q11.oneNextState = q11;
        q11.oneOutputSymbol = 0b10;   
        q11.id = "11";
    }

    public static void main(String[] args) {
        initStates();
        // Random randomizer = new Random();
        // Stream<Integer> stream = randomizer.ints().limit(5).boxed();
        Stream<Integer> stream = IntStream.iterate(0b1001, x -> x + 1).limit(1).boxed();

        List<byte[]> list = stream
                // .peek(x -> System.out.printf("%d, ", x))
                .map(x -> toBinaryDigits(x))
                .map(arr -> codify(arr))
                .collect(Collectors.toList());

        System.out.print("\n");
        list.forEach(x -> binaryPrint(x));
        System.out.print("\n");
    }

    private static byte[] toBinaryDigits(int x) {
        System.out.println("Binarizing: " + x);
        ArrayList<Byte> list = new ArrayList<>();
        while(x > 0) {
            list.add((byte) (x % 2));
            x /= 2;
        }
        System.out.println(list);
        byte[] binaryDigits = new byte[list.size()];
        for(int i = list.size() - 1; i >= 0; --i) {
            binaryDigits[i] = list.get(i);
        }
        System.out.println(Arrays.toString(binaryDigits));
        return binaryDigits.clone();
    }

    private static byte[] codify(byte[] input) {
        byte[] output = new byte[input.length * 2];
        State currentState = q00;

        System.out.print("\n");
        for(int i = 0; i < input.length; ++i) {
            System.out.println("Codifying: " + input[i]+ ", state: " + currentState);

            switch(input[i]) {
                case 0:
                    output[2 * i] = (byte) (currentState.zeroOutputSymbol & 10);
                    output[2 * i + 1] = (byte) (currentState.zeroOutputSymbol & 1);

                    System.out.println("Writing: " + currentState.zeroOutputSymbol);

                    currentState = currentState.zeroNextState;  
                    break;
                case 1:
                    output[2 * i] = (byte) (currentState.oneOutputSymbol & 1);
                    output[2 * i + 1] = (byte) (currentState.oneOutputSymbol & 10);

                    System.out.println("Writing: " + currentState.oneOutputSymbol);

                    currentState = currentState.oneNextState;  
                    break;  
            }
        }
        return output;
    }

    /*private static byte[] codify(byte[] input) {
        byte[] output = new byte[input.length * 2];
        
        int  ff0 = 0, ff1 = 0,
             ff0out = 0, ff1out = 0,
             xor0out = 0, xor1out = 0, xor2out = 0;

        for(int i = 0; i < input.length; ++i) {
            ff0out = ff0 & 1;
            ff0 = (ff0 >> 1) & (input[i] << 1);
            ff1out = ff1 & 1;
            ff1 = (ff1 >> 1) & (ff0out << 1);

            xor0out = input[i] ^ ff0out;
            xor1out = xor0out ^ ff1out;
            xor2out = input[i] ^ ff1out;

            output[2 * i] = (byte) xor1out;
            output[2 * i + 1] = (byte) xor2out;
        }

        return output;
    }*/

    private static void binaryPrint(byte[] arr) {
        for(int i = 0; i < arr.length; ++i) {
            if(arr[i] == 0) 
                System.out.print("0");
            else 
                System.out.print("1");

            if(i % 2 == 1) System.out.print(" ");
        }
    }
}