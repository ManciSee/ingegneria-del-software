/*
  Data una lista di interi positivi, prenderli tre a tre contigui e dire quante di queste terne sono pitagorichr
*/

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class Exer1 {
    
    static int[] terna = { 0, 0, 0 };

    public static void main(String []args){
        ArrayList<Integer> list = new ArrayList<>();
        list.add(3);
        list.add(3);
        list.add(4);
        list.add(5);
        list.add(6);
        list.add(8);
        list.add(10);
        
        List<int[]> result = 
        list.stream()
            .map(x -> creaTerna(x))
            .filter(t -> controllaTerna(t[0], t[1], t[2]))
            .collect(Collectors.toList());
            
        result.forEach(x -> System.out.println("{" + x[0] + ", " + x[1] + ", " + x[2] + "}"));
    }
    
    private static int[] creaTerna(int x) {
        terna[0] = terna[1];
        terna[1] = terna[2];
        terna[2] = x;
        return terna.clone();
    }
    
    private static boolean controllaTerna(int a, int b, int c) {
        if(a <= 0 || b <= 0 || c <= 0)
            return false;
        
        if(isIpotenusa(a, b, c))
            return checkSquares(a, b, c);
        else if(isIpotenusa(b, a, c))
            return checkSquares(b, a, c);
        else
            return checkSquares(c, a, b);
    }
    
    private static boolean isIpotenusa(int ip, int l0, int l1) {
        return ip >= l0 && ip >= l1;
    }
    
    private static boolean checkSquares(int a, int b, int c) {
        return (a * a) == (b * b + c * c);
    }
}
