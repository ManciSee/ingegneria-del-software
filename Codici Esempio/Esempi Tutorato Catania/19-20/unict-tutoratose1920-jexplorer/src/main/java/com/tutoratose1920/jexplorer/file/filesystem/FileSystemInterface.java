package com.tutoratose1920.jexplorer.file.filesystem;

import java.io.IOException;
import java.io.Reader;

public interface FileSystemInterface {
    void openRoot(String path) throws IOException;    

    boolean isFile(String path);
    boolean isDirectory(String path);

    String[] getChildren(String path);
    String getIdentifier(String path);

    Reader getReader(String path) throws IOException;
}